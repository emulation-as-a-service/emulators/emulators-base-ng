FROM phusion/baseimage:0.9.22
cmd ["/bin/bash"]

run echo "deb [trusted=yes] http://winswitch.org/ xenial main" > /etc/apt/sources.list.d/winswitch.list
run apt-add-repository ppa:fengestad/stable

RUN apt-get update --allow-unauthenticated && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes --allow-unauthenticated \
build-essential \
debhelper \
       dh-autoreconf\
       dpkg-dev \
       nasm \
       libx11-dev \
       libxext-dev \
       libxt-dev \
       libxv-dev \
       x11proto-core-dev \
       libaudiofile-dev \
       libpulse-dev \
       libgl1-mesa-dev \ 
       libasound2-dev \
       libcaca-dev \
       libglu1-mesa-dev \
       libxkbcommon-dev \
       xpra=2.3.4-r20525-1 \
       python-pip python-dev python-netifaces  python-avahi  pulseaudio socat

RUN pip install python-uinput

copy emucon-init /usr/bin/
run addgroup --gid 1000 bwfla
run useradd -ms /bin/bash --uid 1000 --gid bwfla bwfla
run adduser bwfla xpra
run sed -i '$s/$/ -nolisten local/' /etc/xpra/conf.d/55_server_x11.conf
run sed -i '$s/-auth *[^ ]*//' /etc/xpra/conf.d/55_server_x11.conf

run mkdir -p /run/user/1000/xpra
run chmod a=rwx /run/user/1000/xpra

run mkdir -p /home/bwfla
run chmod a=rwx /home/bwfla

run apt-get clean
ADD https://gitlab.com/emulation-as-a-service/experiments/fake-clock/-/jobs/artifacts/master/raw/LD_PRELOAD_clock_gettime.so?job=build /usr/local/lib
